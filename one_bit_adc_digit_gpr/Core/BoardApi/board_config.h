/*
 * board_config.h
 *
 *  Created on: Jun 3, 2021
 *      Author: seags
 */

#ifndef BOARDAPI_BOARD_CONFIG_H_
#define BOARDAPI_BOARD_CONFIG_H_

#include "stm32l4xx_hal.h"

extern SPI_HandleTypeDef hspi1;



#define SPI_HANDLE hspi1

// ATTENUATTOR:
#define ATT_SPI_HANDLE		SPI_HANDLE
#define ATTENUATOR_CS_PORT	GPIOB
#define ATTENUATOR_CS_PIN   LD3_Pin


// DAC:
#define DAC_SPI_HANDLE		SPI_HANDLE
#define DAC_SYNC_PORT		GPIOB
#define DAC_SYNC_PIN		GPIO_PIN_6




//FPGA module:

// DAC:
#define FPGA_SPI_HANDLE		SPI_HANDLE


// Set delay counter:
#define START_DELAY_RESET_PORT	GPIOB		//OUT  active reset is high
#define START_DELAY_RESET_PIN   GPIO_PIN_1
#define START_DELAY_CLK_PORT	GPIOA		//OUT
#define START_DELAY_CLK_PIN		GPIO_PIN_8


// SAMPLING START (I FORGOT TO CONNECT IT IN HW !!!)
#define START_RESET_PORT		GPIOB		//out , active reset high
#define START_RESET_PIN			GPIO_PIN_0

#define SAMPLING_DONE_PORT		GPIOB		//IN, active sampling done is 0
#define SAMPLING_DONE_PIN      	GPIO_PIN_4

/*  It is function of SPI and should be read by SPI interface:
#define READ_DATA_CLK_PORT	GPIOA  //OUT
#define READ_DATA_CLK_PIN  GPIO_PIN_5
#define READ_DATA_PORT		GPIOA  //IN
#define READ_DATA_PIN  		GPIO_PIN_6
*/

#define DATA_OUT_EN_PORT	GPIOA  //OUT, data enable ACTIVE 1
#define DATA_OUT_EN_PIN  	GPIO_PIN_11


// Reset address counter:
#define READ_ADDRESS_RESET_PORT	GPIOB  //OUT, ACTIVE RESET HIGH
#define READ_ADDRESS_RESET_PIN  GPIO_PIN_5

// Set module address:
#define READ_ADDRESS_0_PORT	GPIOA  //OUT
#define READ_ADDRESS_0_PIN  GPIO_PIN_1
#define READ_ADDRESS_1_PORT	GPIOA  //OUT
#define READ_ADDRESS_1_PIN  GPIO_PIN_3
#define READ_ADDRESS_2_PORT	GPIOA  //OUT
#define READ_ADDRESS_2_PIN  GPIO_PIN_4







#endif /* BOARDAPI_BOARD_CONFIG_H_ */
