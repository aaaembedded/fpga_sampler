it was created in:

Microsoft Visual Studio Community 2019
Version 16.9.0
VisualStudio.16.Release/16.9.0+31025.194
Microsoft .NET Framework
Version 4.8.04084

Installed Version: Community

ASP.NET and Web Tools 2019   16.9.687.37626
ASP.NET and Web Tools 2019

Azure App Service Tools v3.0.0   16.9.687.37626
Azure App Service Tools v3.0.0

C# Tools   3.9.0-6.21124.20+db94f4cc8c78a7cd8cf9cfdae091158d2ba9d974
C# components used in the IDE. Depending on your project type and settings, a different version of the compiler may be used.

Common Azure Tools   1.10
Provides common services for use by Azure Mobile Services and Microsoft Azure Tools.

IntelliCode Extension   1.0
IntelliCode Visual Studio Extension Detailed Info

Microsoft JVM Debugger   1.0
Provides support for connecting the Visual Studio debugger to JDWP compatible Java Virtual Machines

Microsoft MI-Based Debugger   1.0
Provides support for connecting Visual Studio to MI compatible debuggers

NuGet Package Manager   5.9.0
NuGet Package Manager in Visual Studio. For more information about NuGet, visit https://docs.nuget.org/

ProjectServicesPackage Extension   1.0
ProjectServicesPackage Visual Studio Extension Detailed Info

Syntax Visualizer   1.0
An extension for visualizing Roslyn SyntaxTrees.

TypeScript Tools   16.0.30201.2001
TypeScript Tools for Microsoft Visual Studio

Visual Basic Tools   3.9.0-6.21124.20+db94f4cc8c78a7cd8cf9cfdae091158d2ba9d974
Visual Basic components used in the IDE. Depending on your project type and settings, a different version of the compiler may be used.

Visual F# Tools   16.9.0-beta.21102.9+7ce7132f1459095e635194d09d6f73265352029a
Microsoft Visual F# Tools

Visual Studio Code Debug Adapter Host Package   1.0
Interop layer for hosting Visual Studio Code debug adapters in Visual Studio
