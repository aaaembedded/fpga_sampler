/*
 * api_runtime.h
 *
 *  Created on: Jun 17, 2021
 *      Author: vadvol01
 */

#ifndef BOARDAPI_API_RUNTIME_H_
#define BOARDAPI_API_RUNTIME_H_



#include "stm32l4xx_hal.h"
#include "board_states.h"

uint8_t DAC_ATT_TABLE[] =
{
	50,	49,	48,	47,	46,	45,	45,	44,//1
	43,	42,	41,	41,	40,	39,	38,	38,//2
	37,	36,	36,	35,	34,	33,	33,	32,//3
	31,	31,	30,	29,	29,	28,	27,	27,//4
	26,	25,	25,	24,	24,	23,	22,	22,//5
	21,	21,	20,	20,	19,	19,	18,	17,//6
	17,	16,	16,	15,	15,	14,	14,	13,//7
	13,	13,	12,	12,	11,	11,	10,	10,//8 index 63
	9,	9,	9,	8,	8,	7,	7,	7, //9
	6,	6,	6,	5,	5,	5,	4,	4, //10
	4,	3,	3,	3,	3,	2,	2,	2, //11
	1,	1,	1,	1,	0,	0,	0,	0, //12  0->index 92
	0,	0,	0,	0,	0,	0,	0,	0, //13
	0,	0,	0,	0,	0,	0,	0,	0, //14
	0,	0,	0,	0,	0,	0,	0,	0, //15
	0,	0,	0,	0,	0,	0,	0,	0, //16 index 127
	0,	0,	0,	0,	0,	0,	0,	0, //17
	0,	0,	0,	0,	0,	0,	0,	0, //18
	0,	0,	0,	0,	0,	0,	0,	0, //19
	0,	0,	0,	0,	0,	0,	0,	0, //20
	0,	0,	1,	1,	1,	1,	2,	2, //21 0-> 162
	2,	2,	3,	3,	3,	4,	4,	4, //22
	5,	5,	5,	6,	6,	6,	7,	7, //23
	7,	8,	8,	8,	9,	9,	10,	10,//24
	11,	11,	11,	12,	12,	13,	13,	14,//25
	14,	15,	15,	16,	16,	17,	17,	18,//26
	18,	19,	19,	20,	20,	21,	22,	22,//27
	23,	23,	24,	25,	25,	26,	26,	27,//28
	28,	28,	29,	30,	30,	31,	32,	32,//29
	33,	34,	34,	35,	36,	37,	37,	38,//30
	39,	40,	40,	41,	42,	43,	43,	44,//31
	45,	46,	47,	47,	48,	49,	50, 50 //32
};





void api_runtime_run();
void api_runtime_one_line_sample(uint16_t u16_value);
void api_runtime_one_full_sample(uint16_t u16_value);
void api_runtime_get_att_from_dac(uint32_t u32_dac_counter, uint16_t * pu16_att_out);
//void api_runtime_data_summ(uint8_t * pu8_line_bits_in, uint8_t * pu8_line_byts_out);
void api_runtime_data_summ(uint8_t * pu8_line_bits_in, uint16_t * pu16_line_byts_out);
void api_runtime_clear_data_output_buffer();

void  api_runtime_swap_bytes(uint16_t * pu16_in_data_buffer, uint16_t * pu16_out_data_buffer, uint32_t u32_size);

#endif /* BOARDAPI_API_RUNTIME_H_ */
