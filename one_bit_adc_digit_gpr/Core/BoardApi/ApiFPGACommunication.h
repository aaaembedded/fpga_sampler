/*
 * ApiFPGACommunication.h
 *
 *  Created on: Jun 5, 2021
 *      Author: seags
 */

#ifndef BOARDAPI_APIFPGACOMMUNICATION_H_
#define BOARDAPI_APIFPGACOMMUNICATION_H_

#include "stm32l4xx_hal.h"


#define BIT_DATA_ARRAY 256
#define BYTE_DATA_ARRAY 2048

HAL_StatusTypeDef start_sampling();
void start_sampling_test();

void set_start_sampling_delay(uint16_t u16_value);

void shotr_delay(uint32_t u32_value);

void read_line_of_data(uint16_t u16_value);

void set_block_address(uint8_t u8_value);

void line_data_processing(uint8_t* pu8_in, uint8_t* pu8_out);

void copy_line_data_buffer(uint8_t* pu8_out);

#endif /* BOARDAPI_APIFPGACOMMUNICATION_H_ */
