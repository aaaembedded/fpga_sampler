﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Imaging; // for ImageLockMode
using System.Runtime.InteropServices; // for Marshal
using ZedGraph;
using GeoradarGui.Properties;
using System.Drawing.Drawing2D;

namespace GeoradarGui
{
    public partial class SpectrumForm : BaseDockContent
    {

        public delegate void AddDataToGraphFormControl(PointPairList _pointPairList, double parameter);
        public event AddDataToGraphFormControl addDataToGraphFormControl;

        private static List<List<double>> spec_data; // a list of column pixel values
        private readonly Random _random = new Random();
        private ControlForm controlForm;


        public string VisualisationMode;
        public int GeneratedDataHeightDepth;
        public int GeneratedDataWidth;
        public int InputDataScaleFactor;

        public int ScanStepValue;
        public int DeptPointsValue;

        public int DielectricPermittivity;
        public bool DistanceTimeState;



        Settings settings = Settings.Default;

        int current_line_counter = 0;
        double period_counter = 0;



        public SpectrumForm()
        {
            InitializeComponent();
            VisualisationMode = "green";
            InputDataScaleFactor = 1;

            controlForm = new ControlForm();
            GeneratedDataWidth = 600;
            GeneratedDataHeightDepth = 2048;


            Data_init();
            tmr_DataGenerating.Stop();
            //timer1.Start();
        }

        private void Data_init()
        {
            double tmp;

            // Create List of data-list:
            spec_data = new List<List<double>>();
            for (int x = 0; x < GeneratedDataWidth; x++)
            {
                List<double> column = new List<double>();
                for (int y = 0; y < GeneratedDataHeightDepth; y++)
                {
                    column.Add(0);
                }
                spec_data.Add(column);
            }

            // Add aditional column for storage average data
            List<double> average_column = new List<double>();
            for (int y = 0; y < GeneratedDataHeightDepth; y++)
            {
                average_column.Add(0);
            }
            spec_data.Add(average_column);
        }

        private void DataLineGenerate()
        {
            PointPairList _pointPairList = new PointPairList();
            //PointPair ResultPair = new PointPair(0,0);


            double tmp = 0;
            // Generate new line of data:
            period_counter++;
            for (int y = 0; y < GeneratedDataHeightDepth; y++)
            {
                double num = _random.NextDouble() - 0.5;
                tmp = (Math.Sin(Math.PI * ((double)y) / 90) + 1 + (0.05 * num)) * 2 * Math.Sin(Math.PI * (period_counter) / 180) ;
                PointPair _pointPair = new PointPair(tmp * 500, y);
                //tmp = Math.Sin(Math.PI * ((double)y) / (GeneratedDataHeightDepth/16));
                //tmp = (0.05 * num) + ((double)y/100.0) / ((double)y * (double)y/ 10000.0 + 1);
                //PointPair _pointPair = new PointPair(tmp, y);

                _pointPairList.Add(_pointPair);
            }

            addDataToGraphFormControl(_pointPairList, (double)ScanStepValue);
            
        }


        public void addDataLine(PointPairList _pointPairList)
        {
            // Set how many line will take one line:
            int line_multiplier = settings.AverageWindow;



            ///////////////////////////////////////////////////////////////////////////////

            MyDataFile current_data = new MyDataFile();
            for (int i = 0; i < _pointPairList.Count; i++)
            {
                current_data.data_set[i] = _pointPairList[i].X;
            }
            current_data.myDataSize = _pointPairList.Count;

// Here it save data to file:
//            current_data.SaveAdd("test.test");

            //////////////////////////////////////////////////////////////////////////////




            // Clear extra-line:
            if (current_line_counter == 0)
            {
                for (int y = 0; y < _pointPairList.Count; y++)
                {
                    spec_data[GeneratedDataWidth][y] = 0;
                }
            }

            // Add data-line to extra-line:
            if (current_line_counter < settings.AveragingLineWindow)
            {
                for (int y = 0; y < _pointPairList.Count; y++)
                {
                     spec_data[GeneratedDataWidth][y] = spec_data[GeneratedDataWidth][y] + _pointPairList[y].X;
                }
                current_line_counter++;
            }

            if (current_line_counter >= settings.AveragingLineWindow)
            {
                current_line_counter = 0;
                
                for (int y = 0; y < _pointPairList.Count; y++)
                {
                    spec_data[GeneratedDataWidth - 1][y] = (spec_data[GeneratedDataWidth][y] / (double)settings.AveragingLineWindow);
                }

                for (int x = 0; x < GeneratedDataWidth - line_multiplier; x++)
                {

                    for (int y = 0; y < GeneratedDataHeightDepth; y++)
                    {
                        spec_data[x][y] = spec_data[x + line_multiplier][y];
                    }
                }

                for (int i = 0; i < (line_multiplier - 1); i++)
                {
                    for (int y = 0; y < _pointPairList.Count; y++)
                    {
                        spec_data[GeneratedDataWidth - 2 - i][y] = spec_data[GeneratedDataWidth - 1][y];
                    }
                }

                // Update bitmap picture:                
                Update_bitmap_with_data();
            }
        }


        void Update_bitmap_with_data()
        {
            UInt32 b = 0;
            UInt32 g = 0;
            UInt32 r = 0;
            UInt32 u32_color = 0;
            UInt32 sign_flag = 0;
            double color_step = 0;

            int localGeneratedDataHeightDepth = GeneratedDataHeightDepth;
            Bitmap bitmap = new Bitmap(spec_data.Count - 1, localGeneratedDataHeightDepth, PixelFormat.Format32bppArgb);

            // prepare to access data via the bitmapdata object
            BitmapData bitmapData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly, bitmap.PixelFormat);

            // create a byte array to reflect each pixel in the image
            byte[] pixels = new byte[(bitmapData.Stride * bitmap.Height)];

            int bytePosition = 0;
            // fill pixel array with data
            for (int col = 0; col < spec_data.Count - 1; col++)
            {
                double scaleFactor = (double)InputDataScaleFactor / 100.0;

                for (int row = 0; row < localGeneratedDataHeightDepth; row++)
                {
                    bytePosition =( row * bitmapData.Stride + col*4);
                    double pixelVal = spec_data[col][row] * scaleFactor;
                    switch (VisualisationMode)
                    {
                        case "gray":
                            pixelVal = Math.Max(0, pixelVal);
                            pixelVal = Math.Min(255, pixelVal);

                            b = (uint)(pixelVal); 
                            r = (uint)(pixelVal);
                            g = (uint)(pixelVal);

                            break;
                        case "blue":
                            pixelVal = Math.Max(0, pixelVal);
                            pixelVal = Math.Min(255, pixelVal);
                            u32_color = (uint)pixelVal;

                            b = Math.Min(255, (u32_color * 2));
                            g = (uint)(pixelVal);

                            break;
                        case "green":

                            pixelVal = Math.Max(0, pixelVal);
                            pixelVal = Math.Min(255, pixelVal);
                            u32_color = (uint)pixelVal;

                            g = Math.Min(255, (u32_color * 2));
                            b = (uint)(pixelVal/2);

                            break;
                        case "spectrum":

                            color_step = 2 * Math.PI / (2048.0);

                            b = (UInt32)(127.0 * (1.0 + Math.Sin(pixelVal * color_step - Math.PI / 2.0)));
                            r = (UInt32)(127.0 * (1.0 - Math.Cos(pixelVal * color_step - Math.PI / 2.0)));
                            g = (UInt32)(127.0 * (1.0 + Math.Cos(pixelVal * color_step - Math.PI / 2.0)));

#if false
                            sign_flag = 0;
                            if (pixelVal < 0)

                            { sign_flag = 1; }

                            pixelVal = Math.Abs(pixelVal);
                            pixelVal = Math.Max(0, pixelVal);
                            pixelVal = Math.Min(2047, pixelVal);
                            u32_color = (uint)(pixelVal);
                            /*
                             *  b 0   -> 255 ++
                             *  b 256 -> 511 -- 
                             *  g 256 -> 512 ++
                             *  g 512 -> 767 --
                             *  r 512 -> 767 ++ 
                             *  r 768 -> 1024 --
                             *  b 768 -> 1024 ++
                             */
                            if ((u32_color >= 0) && (u32_color < 256))
                            {
                                b = u32_color;   // Up blue
                                g = 0;
                                r = 0;
                            }
                            else if ((u32_color >= 256) && (u32_color < 512))
                            {
                                b = 255;             // full blue   
                                g = u32_color - 256; // Up green
                                r = 0;
                            }
                            else if ((u32_color >= 512) && (u32_color < 768))
                            {
                                b = 767 - u32_color; // down blue
                                g = 255;             // full green 
                                r = 0;
                            }

                            else if ((u32_color >= 768) && (u32_color < 1024))
                            {
                                r = u32_color - 768;  // Up red
                                g = 255;              // Full green
                                b = 0;
                            }
                            else if ((u32_color >= 1024) && (u32_color < 1280))
                            {
                                g = 1279 - u32_color;  // Down green red
                                r = 255;               // Full red
                                b = 0;
                            }
                            else if ((u32_color >= 1280) && (u32_color < 1536))
                            {
                                b = u32_color - 1280;  // Up blue 
                                r = 255;               // Full red
                                g = 0;
                            }
                            else if ((u32_color >= 1536) && (u32_color < 1792))
                            {
                                g = u32_color - 1536;  // Up blue 
                                r = 255;               // Full red
                                b = 1791 - u32_color;
                            }
                            else if ((u32_color >= 1792) && (u32_color < 2048))
                            {
                                b = u32_color - 1792;  // Up blue 
                                r = 255;               // Full red
                                g = 255;
                            }
                            // End of spectrum
#endif



                            break;
                        default:
                            pixelVal = Math.Max(0, pixelVal);
                            pixelVal = Math.Min(255, pixelVal);

                            b = (uint)(pixelVal);
                            r = (uint)(pixelVal);
                            g = (uint)(pixelVal);
                            break;
                    }

                    if (sign_flag == 1)
                    {
                        UInt32 add_color = (r << 16) + (g << 8) + b;
                        add_color = 0xFFFFFF ^ add_color;
                        b = ~add_color >> 16;
                        r = ~add_color >> 8;
                        g = ~add_color;
                    }
                    pixels[bytePosition] = (byte)b;
                    pixels[bytePosition + 1] = (byte)(g);
                    pixels[bytePosition + 2] = (byte)r; 
                    pixels[bytePosition + 3] = 0xFF;//alpha

                }
            }

            // turn the byte array back into a bitmap
            Marshal.Copy(pixels, 0, bitmapData.Scan0, pixels.Length);
            bitmap.UnlockBits(bitmapData);


            double Hscale = (double)pictureBox1.Size.Height / (double)bitmap.Height;
            double Wscale = (double)pictureBox1.Size.Width / (double)bitmap.Width;

            // Add axis legends:
            Bitmap tempBitmap = new Bitmap(pictureBox2.Size.Width, pictureBox2.Size.Height, bitmap.PixelFormat);
            Graphics flagGraphics = Graphics.FromImage(tempBitmap);
            System.Drawing.Pen myPen;
            myPen = new System.Drawing.Pen(System.Drawing.Color.White);
            // disabled data-picture: flagGraphics.DrawImage(bitmap,0,0);
            //flagGraphics.SmoothingMode = SmoothingMode.AntiAlias;
            //flagGraphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            //flagGraphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

            string depth_value;
            RectangleF rectf = new RectangleF(0, 0, 90, 50); // print place
            double depth_time;

            Brush LocalBrush = Brushes.White;


            if (DistanceTimeState == true)
            {
                flagGraphics.DrawString("metrs", new Font("Tahoma", 12), LocalBrush, rectf);
            }
            else
            {
                flagGraphics.DrawString("nS", new Font("Tahoma", 12), LocalBrush, rectf);
            }



            for (int i = 0; i < 10; i++)
            {
                flagGraphics.DrawLine(myPen, 0, (int)(tempBitmap.Height * (i + 1) / 10), tempBitmap.Width / 2, (int)(tempBitmap.Height * (i + 1) / 10));
                rectf = new RectangleF(0, (tempBitmap.Height * (i + 1) / 10), 90, 50);

                depth_time = ((double)((ScanStepValue * DeptPointsValue * (i + 1) / 10.0) / 1000.0) / 2.0);

                if (DistanceTimeState == true)
                {
                    depth_value = (0.3 * depth_time / Math.Sqrt((double)DielectricPermittivity)).ToString("0.00");
                }
                else
                {
                    depth_value = depth_time.ToString("0.00");
                }

                flagGraphics.DrawString(depth_value, new Font("Tahoma", 12), LocalBrush, rectf);
            }
            flagGraphics.Flush();


            try
            {
                Bitmap resized_data = new Bitmap(bitmap, new Size((int)((double)bitmap.Width * Wscale), (int)((double)bitmap.Height * Hscale)));
                // apply the bitmap to the picturebox
                pictureBox1.Image = resized_data;
                pictureBox2.Image = tempBitmap;
            }
            catch
            {

            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            DataLineGenerate();
        }

        public void SetTimerState(bool state_value)
        {
            if (state_value)
            {
                tmr_DataGenerating.Start();
            }
            else 
            {
                tmr_DataGenerating.Stop();
            }
        }

        public void SavePictureImage()
        {
            SaveFileDialog ofd = new SaveFileDialog();
            ofd.InitialDirectory = "";
            ofd.Filter = "Image Files | *.bmp";
            ofd.DefaultExt = "bmp";
            ofd.RestoreDirectory = true;
            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                pictureBox1.Image.Save(ofd.FileName, ImageFormat.Bmp);
            }

        }


    }

    static class SpectrumExtension
    {
        delegate string SetSpectrumAddSpectrumCallback(ComboBox cmb_colormap);
        /// <summary>
        /// This extension method appends collored text.
        /// </summary>
        public static string getCmbColor(this ComboBox cmb_colormap)
        {
            // If this method was called from the another thread.
            if (cmb_colormap.InvokeRequired)
            {
                var deleg = new SetSpectrumAddSpectrumCallback(getCmbColor);
                
                return (string) cmb_colormap.Invoke(deleg, new object[] { cmb_colormap });
                
            }
            else
            {
                return cmb_colormap.Text;
            }
        }

    }

    class MyDataFile : AppSettings<MyDataFile>
    {
        public int myScanCounter = 1;
        public int myDataSize = 1;
        public string VisualisationMode = "green";
        public bool chkFilter = true;
        public double[] data_set = new double[10000];
    }

}
