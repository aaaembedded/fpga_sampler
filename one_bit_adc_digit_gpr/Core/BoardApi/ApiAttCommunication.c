/*
 * ApiSpiCommunication.c
 *
 *  Created on: May 28, 2021
 *      Author: seags
 */

#include <ApiAttCommunication.h>
#include "main.h"
#include "board_config.h"


//extern SPI_HandleTypeDef hspi1;


void one_attenuator_set_value(uint8_t u8_address, uint8_t u8_value)
{
    /* CLK0...- ....CLK15  */
	/* D0 - D7, A0 - A7    */
	HAL_GPIO_WritePin(ATTENUATOR_CS_PORT, ATTENUATOR_CS_PIN, GPIO_PIN_RESET);
	HAL_SPI_Transmit(&ATT_SPI_HANDLE, &u8_value, 1, 100);
	HAL_SPI_Transmit(&ATT_SPI_HANDLE, &u8_address, 1, 100);
	HAL_GPIO_WritePin(ATTENUATOR_CS_PORT, ATTENUATOR_CS_PIN, GPIO_PIN_SET);
}


void attenuation_set_value(uint16_t u16_value)
{
	uint8_t u8_value_0 = 0;
	uint8_t u8_value_1 = 0;
	uint8_t u8_value_2 = 0;

	if(u16_value > 381) // 381 = 0x7F * 3
	{
		u16_value = 381;
	}

	if(u16_value < 0x80)
	{
		u8_value_0 = (uint8_t)u16_value;
		u8_value_1 = 0;
		u8_value_2 = 0;
	}
	else if((u16_value >= 0x80) && (u16_value < 0xFF))
	{
		u8_value_0 = 0x7F;
		u8_value_1 = (uint8_t)(u16_value - 0x7F);
		u8_value_2 = 0;
	}
	else if((u16_value >= 0xFF) && (u16_value < 0x17E))
	{
		u8_value_0 = 0x7F;
		u8_value_1 = 0x7F;
		u8_value_2 = (uint8_t)(u16_value - 0xFE);
	}

	one_attenuator_set_value(2, u8_value_0);
	one_attenuator_set_value(0, u8_value_1);
	one_attenuator_set_value(1, u8_value_2);
}
