/*
 * ApiSpiCommunication.h
 *
 *  Created on: May 28, 2021
 *      Author: seags
 */

#ifndef BOARDAPI_APIATTCOMMUNICATION_H_
#define BOARDAPI_APIATTCOMMUNICATION_H_

#include "stm32l4xx_hal.h"

void one_attenuator_set_value(uint8_t u8_address, uint8_t u8_value);
void attenuation_set_value(uint16_t u16_value);

#endif /* BOARDAPI_APIATTCOMMUNICATION_H_ */
