/*
 * ApiDac8550.h
 *
 *  Created on: Jun 3, 2021
 *      Author: seags
 */

#ifndef BOARDAPI_APIDAC8550_H_
#define BOARDAPI_APIDAC8550_H_

#include "stm32l4xx_hal.h"
#include "board_config.h"


void dac_write(uint16_t u16_value);
uint16_t u16_revers_bits(uint16_t u16_data);

#endif /* BOARDAPI_APIDAC8550_H_ */
