/*
 * ApiDac8550.c
 *
 *  Created on: Jun 3, 2021
 *      Author: seags
 */


#include "ApiDac8550.h"


void dac_write(uint16_t u16_value)
{
	uint8_t pu8_value[3];

	// Revert input value bits. (It is necessary for compatibility.)
	// u16_value = u16_revers_bits(u16_value);

	// IF SPI MSB
	if(hspi1.Init.FirstBit == SPI_FIRSTBIT_MSB)
	{
		pu8_value[0] = 0;
		pu8_value[1] = (uint8_t)(u16_value >> 8);
		pu8_value[2] = (uint8_t)(u16_value & 0xFF);
	}
	else
	{
	// IF SPI LSB
		u16_value = u16_revers_bits(u16_value);
		pu8_value[0] = 0;
		pu8_value[2] = (uint8_t)(u16_value >> 8);
		pu8_value[1] = (uint8_t)(u16_value & 0xFF);
	}

	HAL_GPIO_WritePin(DAC_SYNC_PORT, DAC_SYNC_PIN, GPIO_PIN_SET);

	// start sync of data frame:
	HAL_GPIO_WritePin(DAC_SYNC_PORT, DAC_SYNC_PIN, GPIO_PIN_RESET);

	// send data, xxxxxx PD1 PD0, DATA16_HIGH, DATA16_XXX... DATA16_LOW
	// PD1, PD0 is config bits. For normal operation must be 0 and 0.
	HAL_SPI_Transmit(&DAC_SPI_HANDLE, &pu8_value[0], 3, 100);

	// stop sync of data frame
	HAL_GPIO_WritePin(DAC_SYNC_PORT, DAC_SYNC_PIN, GPIO_PIN_SET);
}


uint16_t u16_revers_bits(uint16_t u16_data)
{
	uint32_t u32_counter = 0;
	uint16_t u16_rev = 0;

	while(u32_counter < 16)
	{
		u16_rev <<= 1;
	    if ((u16_data & 1) == 1)
	    {
	    	u16_rev ^= 1;
	    }
	    u16_data >>= 1;
		u32_counter++;
	}
	// required number
	return u16_rev;
}




