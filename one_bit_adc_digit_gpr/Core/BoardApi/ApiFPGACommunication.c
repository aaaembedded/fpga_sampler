/*
 * ApiFPGACommunication.c
 *
 *  Created on: Jun 5, 2021
 *      Author: seags
 */

#include "ApiFPGACommunication.h"
#include "board_config.h"


#define SOME_DELAY_VALUE 20000



uint8_t pu8_raw_line_array[BIT_DATA_ARRAY];
uint8_t pu8_bit_line_array[BIT_DATA_ARRAY];
uint32_t u32_summa;

void read_line_of_data(uint16_t u16_value)
{
	uint32_t u32_index = 0;

	uint32_t u32_block_counter = 0;
	uint32_t u32_tmp_index = 0;
	//uint32_t u32_summa = 0;

	//set_start_sampling_delay(u16_value); // base delay

	if( start_sampling() == HAL_OK)
	{
		HAL_GPIO_WritePin(READ_ADDRESS_RESET_PORT,READ_ADDRESS_RESET_PIN,GPIO_PIN_SET);
		HAL_SPI_Receive(&FPGA_SPI_HANDLE, &pu8_raw_line_array[0], 1, 1000);
		HAL_GPIO_WritePin(READ_ADDRESS_RESET_PORT,READ_ADDRESS_RESET_PIN,GPIO_PIN_RESET);

		// Turn DATA OUT
		u32_block_counter = 0;
		while(u32_block_counter < 8)
		{
			set_block_address(u32_block_counter);
			HAL_GPIO_WritePin(DATA_OUT_EN_PORT, DATA_OUT_EN_PIN, GPIO_PIN_SET);
			u32_tmp_index = u32_index + (u32_block_counter * 32u);
			// Read u8
			HAL_SPI_Receive(&FPGA_SPI_HANDLE, &pu8_raw_line_array[u32_tmp_index], 32, 1000);
			// Read U16
			//HAL_SPI_Receive(&FPGA_SPI_HANDLE, &pu8_raw_line_array[u32_tmp_index], 16, 1000);

			u32_block_counter++;
		}

#if 0
		{
			uint32_t u32_index = 0;
			for(u32_index = 0; u32_index < 32; u32_index++ )
			{
				pu8_raw_line_array[32 * 0 + u32_index] = 0x00;
				pu8_raw_line_array[32 * 1 + u32_index] = 0x00;
				pu8_raw_line_array[32 * 2 + u32_index] = 0x00;
				pu8_raw_line_array[32 * 3 + u32_index] = 0x00;
				pu8_raw_line_array[32 * 4 + u32_index] = 0x00;
				pu8_raw_line_array[32 * 5 + u32_index] = 0x00;
				pu8_raw_line_array[32 * 6 + u32_index] = 0x00;
				pu8_raw_line_array[32 * 7 + u32_index] = 0x00;
			}
			pu8_raw_line_array[32*7] = 0x80;


//			pu8_raw_line_array[32 * 0 + 2] = 0x55;
//			pu8_raw_line_array[32 * 1 + 2] = 0x55;
//			pu8_raw_line_array[32 * 2 + 2] = 0x55;
//			pu8_raw_line_array[32 * 3 + 2] = 0x55;
//			pu8_raw_line_array[32 * 4 + 2] = 0x55;
//			pu8_raw_line_array[32 * 5 + 2] = 0x55;
//			pu8_raw_line_array[32 * 6 + 2] = 0x55;
//			pu8_raw_line_array[32 * 7 + 2] = 0x55;
		}
#endif

#if 1
		line_data_processing(&pu8_raw_line_array[0], &pu8_bit_line_array[0]);
#endif

		// ANALIZE
#if 0
		u32_block_counter = 0;
		u32_summa = 0;
		while(u32_block_counter < 256)
		{
			u32_summa = u32_summa + pu8_raw_line_array[u32_block_counter];
			u32_block_counter++;
		}

		if(u32_summa > 0)
		{
			u32_block_counter = 0;

		}
#endif

	}
}


// Function sets received bits to line order:
void line_data_processing(uint8_t* pu8_in, uint8_t* pu8_out)
{
	uint32_t u32_in_index = 0;
	uint32_t u32_out_index = 0;
	uint32_t u32_shift_bits = 0;
	uint32_t u32_shift_index = 0;
	uint8_t current_bit = 0;
	uint32_t u32_tmp_index = 0;
	uint32_t u32_step_counter = 0;

	u32_out_index = 0;
#if 0
	for(u32_shift_index = 0; u32_shift_index < 32; u32_shift_index++)
	{
		for(u32_shift_counter = 0; u32_shift_counter < 8; u32_shift_counter++)
		{
			for(u32_in_index = u32_shift_index; u32_in_index < 256; u32_in_index = u32_in_index + 32)
			{
				pu8_bit_line_array[u32_out_index] = pu8_bit_line_array[u32_out_index] << 1;

				//current_bit = (pu8_raw_line_array[u32_in_index]>> (7 - u32_shift_counter)) & 0x01;
				current_bit = (pu8_raw_line_array[u32_in_index]>> (7)) & 0x01;

				pu8_bit_line_array[u32_out_index] = pu8_bit_line_array[u32_out_index] + current_bit;
			}
			u32_out_index++;
		}
	}
#endif
	u32_out_index = 0;

	//u32_in_index = 255;

	for(u32_step_counter = 0; u32_step_counter < 32; u32_step_counter++ )
	{
		for(u32_shift_index = 0; u32_shift_index < 8; u32_shift_index++)
		{
			for(u32_shift_bits = 0; u32_shift_bits < 8; u32_shift_bits++)
			{
				pu8_bit_line_array[u32_out_index] = pu8_bit_line_array[u32_out_index] << 1;
	//			u32_tmp_index = 255 - u32_step_counter - (32 * u32_shift_bits);

#if 1
				switch (u32_shift_bits)
				{
					case 0:
						u32_in_index = 5;
						break;
					case 1:
						u32_in_index = 4;
						break;
					case 2:
						u32_in_index = 3;
						break;
					case 3:
						u32_in_index = 2;
						break;
					case 4:
						u32_in_index = 1;
						break;
					case 5:
						u32_in_index = 7;
						break;
					case 6:
						u32_in_index = 7; // 7
						break;
					case 7:
						u32_in_index = 6; // 6
						break;

					default:
						break;
				}
				u32_tmp_index = 255 - u32_step_counter - 32 * u32_in_index;//- (32 * u32_shift_bits);
#endif


				current_bit = (pu8_raw_line_array[u32_tmp_index] >> (7 - u32_shift_index)) & 0x01; // SPI LSB
				//current_bit = (pu8_raw_line_array[u32_tmp_index] >> (u32_shift_index)) & 0x01; // SPI MSB
				pu8_bit_line_array[u32_out_index] = pu8_bit_line_array[u32_out_index] + current_bit;
			}

			// move to next index:
			u32_out_index++;
		}
	}
}


void copy_line_data_buffer(uint8_t* pu8_out)
{
	uint32_t u32_index = 0;

	while(u32_index < BIT_DATA_ARRAY)
	{
		pu8_out[u32_index] = pu8_bit_line_array[u32_index];
		u32_index++;
	}
}







void set_block_address(uint8_t u8_value)
{
	switch(u8_value)
	{
		case 0:
			HAL_GPIO_WritePin(READ_ADDRESS_0_PORT, READ_ADDRESS_0_PIN, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(READ_ADDRESS_1_PORT, READ_ADDRESS_1_PIN, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(READ_ADDRESS_2_PORT, READ_ADDRESS_2_PIN, GPIO_PIN_RESET);
			break;
		case 1:
			HAL_GPIO_WritePin(READ_ADDRESS_0_PORT, READ_ADDRESS_0_PIN, GPIO_PIN_SET);
			HAL_GPIO_WritePin(READ_ADDRESS_1_PORT, READ_ADDRESS_1_PIN, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(READ_ADDRESS_2_PORT, READ_ADDRESS_2_PIN, GPIO_PIN_RESET);
			break;
		case 2:
			HAL_GPIO_WritePin(READ_ADDRESS_0_PORT, READ_ADDRESS_0_PIN, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(READ_ADDRESS_1_PORT, READ_ADDRESS_1_PIN, GPIO_PIN_SET);
			HAL_GPIO_WritePin(READ_ADDRESS_2_PORT, READ_ADDRESS_2_PIN, GPIO_PIN_RESET);
			break;
		case 3:
			HAL_GPIO_WritePin(READ_ADDRESS_0_PORT, READ_ADDRESS_0_PIN, GPIO_PIN_SET);
			HAL_GPIO_WritePin(READ_ADDRESS_1_PORT, READ_ADDRESS_1_PIN, GPIO_PIN_SET);
			HAL_GPIO_WritePin(READ_ADDRESS_2_PORT, READ_ADDRESS_2_PIN, GPIO_PIN_RESET);
			break;
		case 4:
			HAL_GPIO_WritePin(READ_ADDRESS_0_PORT, READ_ADDRESS_0_PIN, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(READ_ADDRESS_1_PORT, READ_ADDRESS_1_PIN, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(READ_ADDRESS_2_PORT, READ_ADDRESS_2_PIN, GPIO_PIN_SET);
			break;
		case 5:
			HAL_GPIO_WritePin(READ_ADDRESS_0_PORT, READ_ADDRESS_0_PIN, GPIO_PIN_SET);
			HAL_GPIO_WritePin(READ_ADDRESS_1_PORT, READ_ADDRESS_1_PIN, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(READ_ADDRESS_2_PORT, READ_ADDRESS_2_PIN, GPIO_PIN_SET);
			break;
		case 6:
			HAL_GPIO_WritePin(READ_ADDRESS_0_PORT, READ_ADDRESS_0_PIN, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(READ_ADDRESS_1_PORT, READ_ADDRESS_1_PIN, GPIO_PIN_SET);
			HAL_GPIO_WritePin(READ_ADDRESS_2_PORT, READ_ADDRESS_2_PIN, GPIO_PIN_SET);
			break;
		case 7:
			HAL_GPIO_WritePin(READ_ADDRESS_0_PORT, READ_ADDRESS_0_PIN, GPIO_PIN_SET);
			HAL_GPIO_WritePin(READ_ADDRESS_1_PORT, READ_ADDRESS_1_PIN, GPIO_PIN_SET);
			HAL_GPIO_WritePin(READ_ADDRESS_2_PORT, READ_ADDRESS_2_PIN, GPIO_PIN_SET);
			break;

		default:
	    	break;
	}

}


HAL_StatusTypeDef start_sampling()
{
	HAL_StatusTypeDef errorcode = HAL_OK;
	uint32_t u32_counter = 0;


	HAL_GPIO_WritePin(START_RESET_PORT, START_RESET_PIN, GPIO_PIN_SET);
	//HAL_Delay(1);

	u32_counter = 0;
	// Wait before SAMPLE_DOWN OUT WILL GOING TO RESER STATE(HIGH):
	while(HAL_GPIO_ReadPin(SAMPLING_DONE_PORT, SAMPLING_DONE_PIN) == GPIO_PIN_RESET)
	{
		u32_counter++;
		if(u32_counter == SOME_DELAY_VALUE)
		{
			errorcode = HAL_ERROR;
		}
	}

	if(errorcode == HAL_OK)
	{
		// Start sampling:
		HAL_GPIO_WritePin(START_RESET_PORT, START_RESET_PIN, GPIO_PIN_RESET);

		// wait for "sampling done" signal
		u32_counter = 0;
		while(GPIO_PIN_RESET != HAL_GPIO_ReadPin(SAMPLING_DONE_PORT, SAMPLING_DONE_PIN))
		{
			u32_counter++;
			if(u32_counter == SOME_DELAY_VALUE)
			{
				errorcode = HAL_ERROR;
			}
		}
	}

	return (errorcode);
}


void start_sampling_test()
{
	// Test part:
	uint16_t u16_counter = 0;
	uint32_t u32_delay = 0;

	set_start_sampling_delay(u16_counter);

	while(1)
	{
		HAL_GPIO_WritePin(START_RESET_PORT, START_RESET_PIN, GPIO_PIN_SET);
		HAL_Delay(1);
		// set_start_sampling_delay(u16_counter);

		HAL_GPIO_WritePin(START_RESET_PORT, START_RESET_PIN, GPIO_PIN_RESET);
		HAL_Delay(5);

		if(u32_delay < 100)
		{
			u32_delay++;
		}
		else
		{
			u32_delay = 0;
			u16_counter = u16_counter + 1;
			HAL_GPIO_WritePin(START_RESET_PORT, START_RESET_PIN, GPIO_PIN_SET);
			HAL_Delay(1);
			set_start_sampling_delay(u16_counter);
		}


		if(u16_counter >= 50)
		{
			u16_counter = 0;
		}
	}
}


void set_start_sampling_delay(uint16_t u16_value)
{
	uint16_t u16_counter = 0;

	// Minimal value must be 1, to produce "stop" pulse after "16-Bit Identity Comparator" inside FPGA.
	// Because 0x0000 value "delay counter" has during reset.

	u16_value = u16_value + 1;

	// Reseting counter value procedure:

	// Set RESET to HIGH (go to reset counter value to 0 ):
	HAL_GPIO_WritePin(START_DELAY_RESET_PORT, START_DELAY_RESET_PIN, GPIO_PIN_SET);
	HAL_Delay(1);
	// Set CLK to LOW:
	HAL_GPIO_WritePin(START_DELAY_CLK_PORT, START_DELAY_CLK_PIN, GPIO_PIN_RESET);
	//shotr_delay(100);
	// Set CLK to HIGH:
	HAL_GPIO_WritePin(START_DELAY_CLK_PORT, START_DELAY_CLK_PIN, GPIO_PIN_SET);
	//shotr_delay(100);
	// Set CLK to LOW:
	HAL_GPIO_WritePin(START_DELAY_CLK_PORT, START_DELAY_CLK_PIN, GPIO_PIN_RESET);
	//shotr_delay(100);
	// Set RESET to LOW (inactive state):
	HAL_GPIO_WritePin(START_DELAY_RESET_PORT, START_DELAY_RESET_PIN, GPIO_PIN_RESET);
	//shotr_delay(100);
	while(u16_counter < u16_value)
	{
		// Clock by CLK:
		HAL_GPIO_WritePin(START_DELAY_CLK_PORT, START_DELAY_CLK_PIN, GPIO_PIN_SET);
		// HAL_Delay(1);
		//shotr_delay(100);
		HAL_GPIO_WritePin(START_DELAY_CLK_PORT, START_DELAY_CLK_PIN, GPIO_PIN_RESET);
		// HAL_Delay(1);
		//shotr_delay(100);
		u16_counter++;
	}
}


void shotr_delay(uint32_t u32_value)
{
	volatile uint32_t u32_counter = 0;

	while(u32_counter < u32_value)
	{
		u32_counter++;
	}
}

