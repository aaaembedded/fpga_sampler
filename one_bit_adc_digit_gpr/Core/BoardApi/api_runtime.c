/*
 * api_runtime.c
 *
 *  Created on: Jun 17, 2021
 *      Author: vadvol01
 */


#include "api_runtime.h"
#include "ApiFPGACommunication.h"
#include "ApiAttCommunication.h"
#include "ApiDac8550.h"
#include "AppUartCommunication.h"
#include "main.h"

uint8_t u8_line_out_bits_buffer[BIT_DATA_ARRAY];
uint16_t u16_line_out_data_bytes_buffer[BYTE_DATA_ARRAY];

uint16_t u16_line_out_swap_bytes_buffer[BYTE_DATA_ARRAY];

void api_runtime_run()
{
//	uint32_t u32_runtime_state = 0;
static	uint32_t u32_value = 0;
static	uint32_t u32_delay = 0;
	board_get_uint32_state(START_ON_FLAG_PARAM, &u32_value);
	board_get_uint32_state(STATIC_DELAY_PARAM, &u32_delay);
	u32_delay = u32_delay / 5; // calculation of real value for delay . One step is 5nS.

//	u32_value = 0;


	switch(1)
	{

		case 1:
			api_runtime_one_full_sample(u32_delay);

			break;

		default:
			break;

	}

	HAL_Delay(100);
}


void api_runtime_one_line_sample(uint16_t u16_value)
{
	read_line_of_data(u16_value);
}


void api_runtime_one_full_sample(uint16_t u16_value)
{
	uint16_t u16_att_out = 0;
	uint32_t u32_dac_counter = 0;
	int16_t i16_current_dac_value = 0;
	// Set DAC level:

	u32_dac_counter = 0;

	// Clear output buffer before sampling:
	api_runtime_clear_data_output_buffer();
	i16_current_dac_value = -5376; //-32767;

	//set_start_sampling_delay(2); // base delay
	set_start_sampling_delay(u16_value); // base delay
	while(1)
	{
		//set_start_sampling_delay(u16_value); // base delay
		dac_write(i16_current_dac_value);
		//dac_write(0);
		i16_current_dac_value = i16_current_dac_value + 42;// 256; //42 is a step for 0.54v range

		// Set Att level:
		api_runtime_get_att_from_dac(u32_dac_counter, &u16_att_out);
    //    attenuation_set_value(u16_att_out);

		attenuation_set_value(0);//

		//one_attenuator_set_value(0, 10);
		//one_attenuator_set_value(1, 10);
		//one_attenuator_set_value(2, 10);
		//HAL_Delay(1);

        HAL_GPIO_WritePin(GPIOB, LD3_Pin, GPIO_PIN_RESET);
		read_line_of_data(u16_value);


		// data processing:
		// copy line bit buffer to u8_line_out_bits_buffer:
		copy_line_data_buffer(&u8_line_out_bits_buffer[0]);

		// Summing each bit to corresponding byte:
		api_runtime_data_summ(&u8_line_out_bits_buffer[0], &u16_line_out_data_bytes_buffer[0]);

		u32_dac_counter++;
		if(u32_dac_counter > 255)
		{

			HAL_GPIO_WritePin(GPIOB, LD3_Pin, GPIO_PIN_SET);

			//api_runtime_swap_bytes(&u16_line_out_data_bytes_buffer[0], &u16_line_out_swap_bytes_buffer[0], 2048);
			//api_send_data( &u16_line_out_swap_bytes_buffer[0], 4096, GEORADAR_DATA_PACKET_ID, 0);

			api_send_data(&u16_line_out_data_bytes_buffer[0], 4096, GEORADAR_DATA_PACKET_ID, 0);

			break;
		}
	}
}


void  api_runtime_swap_bytes(uint16_t * pu16_in_data_buffer, uint16_t * pu16_out_data_buffer, uint32_t u32_size)
{
	uint32_t u32_byte_index = 0;
	uint32_t u32_four_index = 0;
	uint32_t u32_tmp_index = 0;
	uint32_t u32_out_index = 0;

	while(u32_byte_index < u32_size)
	{
		u32_out_index = (u32_four_index * 4) + ((u32_four_index * 4) + 3 - u32_byte_index);
		pu16_out_data_buffer[u32_out_index] = pu16_in_data_buffer[u32_byte_index];
		u32_byte_index = u32_byte_index + 1;
		u32_tmp_index++;
		if(u32_tmp_index >= 4)
		{
			u32_four_index++;
			u32_tmp_index = 0;
		}
	}
}


void api_runtime_data_summ(uint8_t * pu8_line_bits_in, uint16_t * pu16_line_byts_out)
{
	uint32_t u32_byte_index = 0;
	uint32_t u32_bit_index = 0;
	uint32_t u32_full_index = 0;

	while(u32_byte_index < 256)
	{
		u32_bit_index = 0;
		while(u32_bit_index < 8)
		{
pu16_line_byts_out[u32_full_index] = pu16_line_byts_out[u32_full_index] + ((pu8_line_bits_in[u32_byte_index] >> (7-u32_bit_index)) &0x01);
			u32_full_index++;
			u32_bit_index++;
		}
		u32_byte_index++;
	}
}



void api_runtime_get_att_from_dac(uint32_t u32_dac_counter, uint16_t * pu16_att_out)
{

	// TODO: add table conversion:
	*pu16_att_out = 0;
	if(u32_dac_counter > 255)
	{
		u32_dac_counter = 255;
	}

	// One step is 0.25dB, so , one dB is 4 steps
	*pu16_att_out  = 4 * ((uint16_t)DAC_ATT_TABLE[u32_dac_counter]);

#if 0
	if(u32_dac_counter >= 128)
	{
		u32_dac_counter = u32_dac_counter - 128;
	}
	else
	{
		u32_dac_counter = 128 - u32_dac_counter;
	}
	*pu16_att_out  = (uint16_t)u32_dac_counter * 2;
#endif
}


void api_runtime_clear_data_output_buffer()
{
	uint32_t u32_index = 0;

	while(u32_index < BYTE_DATA_ARRAY)
	{
		u16_line_out_data_bytes_buffer[u32_index] = 0;
		u32_index++;
	}
}




